import { Sequelize } from "sequelize-typescript";
import { ENV_MYSQL_DB, ENV_MYSQL_HOSTNAME, ENV_MYSQL_PASSWORD, ENV_MYSQL_USER } from "../util/secrets.util";
import logger from "../util/logger.util";
import { QueryOptions } from "sequelize";
import { User } from "../models/user.model";
import { Team } from "../models/team.model";
import { PasswordReset } from "../models/password-reset.model";
import { Attachment } from "../models/attachment.model";
import { PreUser } from "../models/pre-user.model";

class DBService {
  private _sequelize: Sequelize;

  private constructor() {
    logger.silly("[GD] DBService");
    this._sequelize = new Sequelize({
      dialect : "mysql",
      host    : ENV_MYSQL_HOSTNAME,
      database: ENV_MYSQL_DB,
      username: ENV_MYSQL_USER,
      password: ENV_MYSQL_PASSWORD,
    });

    this._sequelize.addModels([
      User,
      Team,
      PasswordReset,
      Attachment,
      PreUser
    ]);
  }

  static getInstance(): DBService {
    return new DBService();
  }

  async rawQuery(sql: string | { query: string, values: any[] }, options?: QueryOptions): Promise<any> {
    return this._sequelize.query(sql, options);
  }
}

export const dbService = DBService.getInstance();
