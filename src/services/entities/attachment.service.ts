import logger from "../../util/logger.util";
import { AttachmentCreateDto } from "../../dtos/attachment/attachment-create.dto";
import { Attachment } from "../../models/attachment.model";

class AttachmentService {
  constructor() {
    logger.silly("[N-IB] AttachmentService");
  }

  static getInstance(): AttachmentService {
    return new AttachmentService();
  }

  async create(data: AttachmentCreateDto) {
    return Attachment.create(data);
  }

  async show(attachmentId: number) {
    return Attachment.findOne({
      where: {
        id: attachmentId
      }
    });
  }

  async delete(attachment: Attachment) {
    await attachment.destroy();
  }
}

export const attachmentService = AttachmentService.getInstance();