import logger from "../../util/logger.util";
import { UserCreateDto } from "../../dtos/user/user-create.dto";
import { isUndefined } from "util";
import { UserUpdateDto } from "../../dtos/user/user-update.dto";
import { Helpers } from "../../util/helpers.util";
import { User } from "../../models/user.model";
import { PasswordReset } from "../../models/password-reset.model";
import { PreUser } from "../../models/pre-user.model";

class UserService {
  private readonly LIMIT = 20;

  private constructor() {
    logger.silly("[N-GD] UserService");
  }

  static getInstance(): UserService {
    return new UserService();
  }

  async preSignup(data: { email: string }): Promise<PreUser> {
    const token = Helpers.generateRandomString(8, {
      includeLowerCase        : true,
      includeNumbers          : true,
      includeUpperCase        : true
    });
    console.log(token);
    return PreUser.create({
      email: data.email,
      token: token
    });
  }

  async create(data: UserCreateDto): Promise<User> {
    // if (isUndefined(data.description)) {
    //     data.description = null;
    // }
    //
    // if (isUndefined(data.last_name)) {
    //     data.last_name = null;
    // }
    //
    // if (isUndefined(data.manager_id)) {
    //     data.manager_id = null;
    // }
    //
    // if (isUndefined(data.phone_number)) {
    //     data.phone_number = null;
    // }

    return User.create(data);
  }

  async show(userId: number, withIncludes?: boolean): Promise<User> {
    return User.findOne({
      where  : {
        id: userId
      },
      include: withIncludes ? [
        {all: true}
      ] : []
    });
  }

  async showUserByEmail(email: string, withIncludes?: boolean): Promise<User> {
    return User.findOne({
      where  : {
        email: email
      },
      include: withIncludes ? [
        {all: true}
      ] : []
    });
  }

  async showUserByMobile(mobile: string, withIncludes?: boolean): Promise<User> {
    return User.findOne({
      where  : {
        mobile_no: mobile
      },
      include: withIncludes ? [
        {all: true}
      ] : []
    });
  }

  async update(user: User, data: UserUpdateDto): Promise<User> {
    Helpers.removeUndefinedKeys(data);
    return user.update(data);
  }

  async delete(user: User): Promise<any> {
    await user.destroy();
  }

  async showPasswordResetByToken(token: string): Promise<PasswordReset> {
    return PasswordReset.findOne({
      where: {
        token: token,
      }
    });
  }

  async forgotPassword(user: User): Promise<string> {
    const token = await this.generateResetToken(user);
    console.log(token);
    // TODO: send email
    return token;
  }


  private async generateResetToken(user: User): Promise<string> {
    const token = Helpers.generateRandomString(10, {
      includeUpperCase        : true,
      includeLowerCase        : true,
      includeNumbers          : true
    });

    const row = await PasswordReset.findOne({
      where: {
        email: user.email
      }
    });

    if (row) {
      await row.update({"token": token});
    } else {
      await PasswordReset.create({
        email: user.email,
        token: token
      });
    }

    return token;
  }

}

export const userService = UserService.getInstance();
