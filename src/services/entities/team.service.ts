import logger from "../../util/logger.util";
import { TeamCreateDto } from "../../dtos/team/team-create.dto";
import { Team } from "../../models/team.model";
import { TeamUpdateDto } from "../../dtos/team/team-update.dto";

class TeamService {
  constructor() {
    logger.silly("[N-IB] TeamService");
  }

  static getInstance(): TeamService {
    return new TeamService();
  }

  async list() {
    return Team.findAll();
  }

  async create(data: TeamCreateDto) {
    let team       = await Team.create(data);
    data.team_code = "INIT-" + (team.id + 100);
    team           = await team.update({team_code: data.team_code});
    return team;
  }

  async show(teamId: number) {
    return await Team.findOne({
      where: {
        id: teamId
      }
    });
  }

  async showByLeader(leaderId: number) {
    return await Team.findOne({
      where: {
        leader_id: leaderId
      }
    });
  }

  async update(team: Team, data: TeamUpdateDto) {
    return await team.update(data);
  }
}

export const teamService = TeamService.getInstance();