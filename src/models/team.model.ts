import {
  AllowNull,
  AutoIncrement, BelongsTo,
  Column,
  DataType, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { User } from "./user.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "teams"
})
export class Team extends Model<Team> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  team_code?: string;

  @Column(DataType.STRING)
  name: string;

  @Column(DataType.STRING)
  topic?: string;

  @Column(DataType.INTEGER)
  marks: number;

  @ForeignKey(() => User)
  @Column(DataType.BIGINT)
  leader_id: number;
}

