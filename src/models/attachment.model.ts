import {
  AllowNull,
  AutoIncrement, BelongsTo,
  Column,
  DataType, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Team } from "./team.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "attachments"
})
export class Attachment extends Model<Attachment> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  type: string;

  @Column(DataType.STRING)
  attachment_url: string;

  @ForeignKey(() => Team)
  @Column(DataType.BIGINT)
  team_id: number;
}

