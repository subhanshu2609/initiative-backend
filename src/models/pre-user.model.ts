import {
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  DataType, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { UserBranch } from "../enums/user-branch.enum";
import { UserYear } from "../enums/user-year.enum";
import { UserRole } from "../enums/user-role.enum";
import { genSaltSync, hashSync } from "bcrypt";
import { Team } from "./team.model";

@Table({
  timestamps: true,
  paranoid  : true,
  tableName : "pre-users"
})
export class PreUser extends Model<PreUser> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.STRING)
  email: string;

  @Column(DataType.STRING)
  token: string;

}
