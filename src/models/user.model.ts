import {
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  DataType, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { UserBranch } from "../enums/user-branch.enum";
import { UserYear } from "../enums/user-year.enum";
import { UserRole } from "../enums/user-role.enum";
import { genSaltSync, hashSync } from "bcrypt";
import { Team } from "./team.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "users"
})
export class User extends Model<User> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  name: string;

  @Unique
  @Column(DataType.STRING)
  email: string;

  @Column({
    type: DataType.STRING,
    set : function (this: User, value: string) {
      this.setDataValue("password", hashSync(value, genSaltSync(2)));
    }
  })
  password?: string;

  @Column(DataType.STRING)
  team_code?: string;

  @Unique
  @Column(DataType.STRING)
  mobile_no: string;

  @Unique
  @Column(DataType.STRING)
  roll_no: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<UserBranch>(UserBranch)}))
  branch: UserBranch;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<UserYear>(UserYear)}))
  year: UserYear;

  @Column(DataType.STRING)
  college: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<UserRole>(UserRole)}))
  role: UserRole;

  @Column(DataType.BIGINT)
  leader_id?: number;

  @ForeignKey(() => Team)
  @Column(DataType.BIGINT)
  team_id?: number;

  @HasMany(() => User, "leader_id")
  members: User[];

  @BelongsTo(() => Team)
  team: Team;
}
