import { AutoIncrement, Column, DataType, Model, PrimaryKey, Table, Unique } from "sequelize-typescript";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "password_resets"
})
export class PasswordReset extends Model<PasswordReset> {

  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  email: string;

  @Column(DataType.STRING)
  token: string;
}
