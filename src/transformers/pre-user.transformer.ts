import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { PreUser } from "../models/pre-user.model";

export class PreUserTransformer extends TransformerAbstract<PreUser> {
  protected _map(preUser: PreUser): Dictionary<any> {
    return {
      id        : preUser.id,
      email     : preUser.email,
      created_at: preUser.createdAt,
      updated_at: preUser.updatedAt
    };
  }

}
