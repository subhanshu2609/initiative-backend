import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Team } from "../models/team.model";

export class TeamTransformer extends TransformerAbstract<Team> {
  protected _map(team: Team): Dictionary<any> {
    return {
      id        : team.id,
      name      : team.name,
      topic     : team.topic,
      leader_id : team.leader_id,
      team_code : team.team_code,
      created_at: team.createdAt,
      updated_at: team.updatedAt
    };
  }

}
