import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { User } from "../models/user.model";
import { isUndefined } from "util";
import { Team } from "../models/team.model";
import { TeamTransformer } from "./team.transformer";

export class UserCompactTransformer extends TransformerAbstract<User> {
  protected _map(user: User): Dictionary<any> {
    return {
      id        : user.id,
      name      : user.name,
      email     : user.email,
      team_code : user.team_code,
      mobile_no : user.mobile_no,
      roll_no   : user.roll_no,
      branch    : user.branch,
      year      : user.year,
      college   : user.college,
      role      : user.role,
      created_at: user.createdAt,
      updated_at: user.updatedAt
    };
  }
}

export class UserTransformer extends TransformerAbstract<User> {

  defaultIncludes = ["members", "team"];

  async includeMembers(user: User): Promise<Dictionary<any>> {
    let members = user.members;
    if (isUndefined(members)) {
      members = await user.$get("members") as User[];
    }
    return await new UserCompactTransformer().transformList(members);
  }

  async includeTeam(user: User): Promise<Dictionary<any>> {
    let team = user.team;
    if ((isUndefined(team))) {
      team = await user.$get("team") as Team;
    }
    return await new TeamTransformer().transform(team);
  }

  protected _map(user: User): Dictionary<any> {
    return {
      id        : user.id,
      name      : user.name,
      email     : user.email,
      team_code : user.team_code,
      mobile_no : user.mobile_no,
      roll_no   : user.roll_no,
      branch    : user.branch,
      year      : user.year,
      college   : user.college,
      role      : user.role,
      created_at: user.createdAt,
      updated_at: user.updatedAt
    };
  }

}
