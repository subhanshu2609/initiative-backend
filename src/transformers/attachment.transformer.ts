import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Attachment } from "../models/attachment.model";

export class AttachmentTransformer extends TransformerAbstract<Attachment> {
  protected _map(attachment: Attachment): Dictionary<any> {
    return {
      id            : attachment.id,
      type          : attachment.type,
      attachment_url: attachment.attachment_url,
      team_code     : attachment.team_id,
      created_at    : attachment.createdAt,
      updated_at    : attachment.updatedAt
    };
  }

}
