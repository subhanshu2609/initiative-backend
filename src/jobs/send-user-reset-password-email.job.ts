import { BaseJob } from "./base.job";
import logger from "../util/logger.util";
import { userService } from "../services/entities/user.service";
import { mailgunService } from "../services/factories/mailgun.service";
import { ENV_DASHBOARD_URL } from "../util/secrets.util";

export class SendUserResetPasswordEmailJob extends BaseJob {
  private email: string;
  private resetToken: string;

  private constructor() {
    super();
  }

  static create(email: string, token: string): SendUserResetPasswordEmailJob {
    const resetPasswordEmailJob      = new SendUserResetPasswordEmailJob();
    resetPasswordEmailJob.email      = email;
    resetPasswordEmailJob.resetToken = token;
    return resetPasswordEmailJob;
  }

  async handle() {
    logger.debug("[SendUserResetPasswordEmailJob] was executed");
    const data = {
      reset_link: `${ENV_DASHBOARD_URL}/auth/reset-password?token=${this.resetToken}`
    };
    await mailgunService.send(this.email, "Reset Password", "reset-password-template.hbs", data);
  }
}
