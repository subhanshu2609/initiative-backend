import { Request, Response } from "express";
import { teamService } from "../services/entities/team.service";
import { TeamNotFoundException } from "../exceptions/team/team-not-found.exception";
import { TeamUpdateDto } from "../dtos/team/team-update.dto";
import { TeamTransformer } from "../transformers/team.transformer";
import { UserRole } from "../enums/user-role.enum";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";

export class TeamController {

  static async listTeams(req: Request, res: Response) {
    const user = req.user;

    if (user.role != UserRole.ADMIN) {
      throw new UnauthorizedException("You are Not Authorized to perform this action", 400);
    }
    const teams = await teamService.list();
    return res.json({
      data: await new TeamTransformer().transformList(teams)
    });
  }

  static async showTeam(req: Request, res: Response) {
    const user = req.user;

    const team = await teamService.show(user.team_id);
    return res.json({
      data: await new TeamTransformer().transform(team)
    });
  }

  static async updateMyTeam(req: Request, res: Response) {
    const user      = req.user;
    const inputData = req.body as TeamUpdateDto;
    const team      = await teamService.showByLeader(user.id);
    if (!team) {
      throw new TeamNotFoundException();
    }
    const updatedTeam = await teamService.update(team, inputData);
    return res.json({
      data: await new TeamTransformer().transform(updatedTeam)
    });
  }

  static async updateTeam(req: Request, res: Response) {
    const teamId    = +req.params.teamId;
    const inputData = req.body as TeamUpdateDto;
    const team      = await teamService.show(teamId);
    if (!team) {
      throw new TeamNotFoundException();
    }
    const updatedTeam = await teamService.update(team, inputData);
    return res.json({
      data: await new TeamTransformer().transform(updatedTeam)
  });
  }
}