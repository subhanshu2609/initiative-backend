import { NextFunction, Request, Response } from "express";
import { userService } from "../services/entities/user.service";
import { UserTransformer } from "../transformers/user.transformer";
import { UserCreateDto } from "../dtos/user/user-create.dto";
import jwt from "jsonwebtoken";
import { compareSync } from "bcrypt";
import { UserUpdateDto } from "../dtos/user/user-update.dto";
import { teamService } from "../services/entities/team.service";
import { UserRole } from "../enums/user-role.enum";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { UserNotFoundException } from "../exceptions/user/user-not-found.exception";
import { queueService } from "../services/factories/queue-factory.service";
import { SendUserResetPasswordEmailJob, SendUserWelcomeEmailJob } from "../jobs";
import { User } from "../models/user.model";
import { TeamCreateDto } from "../dtos/team/team-create.dto";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { UserLoginValidator } from "../validator/user/user-login.validator";
import { UserCreateValidator } from "../validator/user/user-create.validator";
import { CannotAddMemberException } from "../exceptions/user/cannot-add-member.exception";
import { UserUpdateValidator } from "../validator/user/user-update.validator";
import { ForgotPasswordValidator } from "../validator/user/forgot-password.validator";
import { ResetPasswordValidator } from "../validator/user/reset-password.validator";
import { ChangePasswordValidator } from "../validator/user/change-password.validator";
import { PreUserCreateValidator } from "../validator/user/pre-user-create.validator";
import { SendUserCreateEmailJob } from "../jobs";
import { PreUserTransformer } from "../transformers/pre-user.transformer";
import { PreUser } from "../models/pre-user.model";

export class UserController {

    static async show(req: Request, res: Response) {
        const userId = +req.params.userId;
        const user = await userService.show(userId);

        if (!user) {
            throw new UserNotFoundException();
        }

        return res.json({
            data: await new UserTransformer().transform(user)
        });
    }

    static async postSignup(req: Request, res: Response) {
        const email = req.body as { email: string };
        try {
            await (new PreUserCreateValidator().validate(email));
        } catch (e) {
            throw new UnprocessableEntityException(e);
        }

        const preUser = await PreUser.findOne({
            where: {
                email: email.email
            }
        });

        if (preUser) {
            throw new UnauthorizedException("An email has already been send to your email.", 402);
        }

        const leader = await userService.showUserByEmail(email.email);

        if (leader) {
            throw  new UnauthorizedException("You have already Registered", 402);
        }

        const user = await userService.preSignup(email);
        console.log("user", user);
        await queueService.dispatch(SendUserCreateEmailJob.create(user.email, user.token));
        return res.json({
            data: await new PreUserTransformer().transform(user)
        });
    }

    static async authenticate(req: Request, res: Response, next: NextFunction) {
        const inputData = req.body as { emailOrPhone: string, password: string };
        try {
            await (new UserLoginValidator().validate(inputData));
        } catch (e) {
            throw new UnprocessableEntityException(e);
        }
        let user: User;
        if (inputData.emailOrPhone.indexOf("@") !== -1) {
            user = await userService.showUserByEmail(inputData.emailOrPhone, true);
        } else {
            user = await userService.showUserByMobile(inputData.emailOrPhone, true);
        }
        if (!user) {
            throw new UserNotFoundException();
        }
        const isPasswordCorrect = compareSync(inputData.password, user.password);

        if (!isPasswordCorrect) {
            res.status(403);
            return res.json({
                message: "Wrong Password"
            });
        }

        return res.json({
            token: jwt.sign({user}, "secret"),
            user: await (new UserTransformer()).transform(user),
        });

    }


    static async signup(req: Request, res: Response, next: NextFunction) {
        const inputData = req.body as UserCreateDto;
        const token = req.query;

        const preUser = await PreUser.findOne({
            where: {
                token: token.token
            }
        });
        console.log("token", inputData.email);
        console.log("preUser", preUser.email);

        if (!preUser) {
            throw new UnauthorizedException("You are Not Authorized", 401);
        }

        if (inputData.email != preUser.email) {
            throw new UserNotFoundException();
        }

        try {
            await (new UserCreateValidator().validate(inputData));
        } catch (e) {
            throw new UnprocessableEntityException(e);
        }
        const user = await userService.showUserByEmail(inputData.email);
        if (!!user) {
            res.status(409);
            return res.json({
                "message": "User Already Exists",
            });
        }
        inputData.role = UserRole.LEADER;
        let newUser = await userService.create(inputData);
        const team = await teamService.create({
            name: inputData.team_name,
            leader_id: newUser.id,
            topic: inputData.topic_name
        } as TeamCreateDto);
        newUser = await userService.update(newUser, {
            team_code: team.team_code,
            team_id: team.id
        });
        await queueService.dispatch(SendUserWelcomeEmailJob.create(newUser.name, newUser.email, team.name, team.team_code));
        await preUser.destroy();
        return res.json({
            token: jwt.sign({newUser}, "secret"),
            user: await (new UserTransformer()).transform(newUser),
        });
    }

    static async me(req: Request, res: Response) {
        return res.json({
            user: await (new UserTransformer()).transform(req.user),
        });
    }

    static async updateMe(req: Request, res: Response) {
        const user = req.user;
        const body = req.body as UserUpdateDto;

        try {
            await new UserUpdateValidator().validate(body);
        } catch (e) {
            throw new UnprocessableEntityException(e);
        }

        const updatedUser = await userService.update(user, body);

        return res.json({
            user: await (new UserTransformer()).transform(updatedUser)
        });
    }

    static async updateMember(req: Request, res: Response) {
        const memberId = +req.params.memberId;
        const user = req.user;
        const inputData = req.body as UserUpdateDto;
        const member = await userService.show(memberId);

        if (!member.leader_id) {
            throw new UserNotFoundException();
        }

        if (member.leader_id != user.id) {
            throw new UnauthorizedException("You cannot Update Member", 107);
        }

        const updatedMember = await userService.update(member, inputData);

        return res.json({
            data: await new UserTransformer().transform(updatedMember)
        });
    }

    static async addMember(req: Request, res: Response) {
        let user = req.user;
        if (user.role != UserRole.LEADER) {
            throw new UnauthorizedException("You are not authorized to perform this action", 401);
        }
        const inputData = req.body as UserCreateDto;

        try {
            await new UserCreateValidator().validate(inputData);

        } catch (e) {
            throw new UnprocessableEntityException(e);
        }
        const member = await userService.showUserByEmail(inputData.email);

        if (!!member) {
            res.status(409);
            return res.json({
                "message": "Email is linked with another user",
            });
        }
        user = await userService.show(user.id, true);
        if (user.members.length > 1) {
            throw new CannotAddMemberException();
        }
        inputData.role = UserRole.MEMBER;
        inputData.leader_id = user.id;
        let newMember = await userService.create(inputData);
        newMember = await userService.update(newMember, {
            team_code: user.team_code,
            team_id: user.team_id
        });
        await user.reload();
        return res.json({
            leader: await new UserTransformer().transform(user)
        });
    }

    static async deleteMember(req: Request, res: Response) {
        const memberId = +req.params.memberId;
        const member = await userService.show(memberId);
        if (!member) {
            throw new UserNotFoundException();
        }
        await userService.delete(member);
        return res.json("success");
    }

    static async forgotPassword(req: Request, res: Response, next: NextFunction) {
        const inputData = req.body as { emailOrPhone?: string };

        try {
            const validation = await new ForgotPasswordValidator().validate(inputData);

        } catch (e) {
            throw new UnprocessableEntityException(e);
        }

        let user: User;
        if (inputData.emailOrPhone.indexOf("a") !== -1) {
            user = await userService.showUserByEmail(inputData.emailOrPhone, true);
        } else {
            user = await userService.showUserByMobile(inputData.emailOrPhone, true);
        }

        if (!user) {
            res.status(404);
            return res.json({
                message: "No User with this email found."
            });
        }

        const resetToken = await userService.forgotPassword(user);
        res.status(200).json({resetToken});

        await queueService.dispatch(SendUserResetPasswordEmailJob.create(user.email, resetToken));
    }

    static async resetPasswordByCode(req: Request, res: Response, next: NextFunction) {
        const inputData = req.body as { password: string };
        const token = req.query.token;

        try {
            await new ResetPasswordValidator().validate(inputData);

        } catch (e) {
            throw new UnprocessableEntityException(e);
        }


        const resetToken = await userService.showPasswordResetByToken(token);

        if (!resetToken) {
            res.status(422);
            return res.json({
                message: "Invalid Verification Code."
            });
        }

        let user = await userService.showUserByEmail(resetToken.email);

        if (!user) {
            res.status(404);
            return res.json({
                message: "User not Found"
            });
        }

        user = await user.update({password: inputData.password});
        await resetToken.destroy();

        return res.json({
            token: jwt.sign({user}, "secret", {expiresIn: 600}),
            user: await (new UserTransformer()).transform(user),
        });

    }

    static async changePassword(req: Request, res: Response, next: NextFunction) {
        const inputData = req.body as { old_password: string, new_password: string };
        const user = req.user;

        try {
            const validation = await new ChangePasswordValidator().validate(inputData);

        } catch (e) {
            throw new UnprocessableEntityException(e);
        }

        const isPasswordCorrect = compareSync(inputData.old_password, user.password);
        if (!isPasswordCorrect) {
            res.status(403);
            return res.json({
                message: "Wrong Password"
            });
        }
        await user.update({password: inputData.new_password});
        return res.status(200).json("success");
    }
}
