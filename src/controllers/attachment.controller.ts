import { Request, Response } from "express";
import { AttachmentCreateDto } from "../dtos/attachment/attachment-create.dto";
import { attachmentService } from "../services/entities/attachment.service";
import { AttachmentTransformer } from "../transformers/attachment.transformer";
import { AttachmentNotFoundException } from "../exceptions/attachment/attachment-not-found.exception";

export class AttachmentController {

  static async createAttachment(req: Request, res: Response) {
    const user        = req.user;
    const inputData   = req.body as AttachmentCreateDto;
    inputData.team_id = user.team_id;
    const attachment  = await attachmentService.create(inputData);
    return res.json({
      data: await new AttachmentTransformer().transform(attachment)
    });
  }

  static async deleteAttachment(req: Request, res: Response) {
    const attachmentId = +req.params.attachmentId;
    const attachment   = await attachmentService.show(attachmentId);
    if (!attachment) {
      throw new AttachmentNotFoundException();
    }
    await attachmentService.delete(attachment);
    return res.json("success");
  }
}