export enum UserRole {
  ADMIN  = "admin",
  MEMBER = "member",
  LEADER = "leader"
}
