export enum UserBranch {
    CS    = "cs",
    IT    = "it",
    ECE   = "ece",
    CE    = "ce",
    ME    = "me",
    EN    = "en",
    EI    = "ei",
    MCA   = "mca",
    OTHER = "other"
}
