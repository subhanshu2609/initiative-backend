import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class TeamNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Team Not Found!", ApiErrorCode.TEAM_NOT_FOUND);
  }
}