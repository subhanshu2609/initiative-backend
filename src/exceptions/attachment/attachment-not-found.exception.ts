import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class AttachmentNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Attachment Not Found!", ApiErrorCode.ATTACHMENT_NOT_FOUND);
  }
}