import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class CannotAddMemberException extends ModelNotFoundException {

  constructor() {
    super("Member Cant be added!", ApiErrorCode.CANNOT_ADD_MEMBER);
  }
}