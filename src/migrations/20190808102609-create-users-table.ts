import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { UserYear } from "../enums/user-year.enum";
import { UserBranch } from "../enums/user-branch.enum";
import { UserRole } from "../enums/user-role.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("users", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      name     : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      email    : {
        type     : Sequelize.STRING,
        unique   : true,
        allowNull: false
      },
      password : {
        type     : Sequelize.STRING,
        allowNull: true,
      },
      team_code     : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      mobile_no: {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true,
      },
      roll_no  : {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true
      },
      branch   : {
        type     : Sequelize.STRING,
        allowNull: true,
        values   : Helpers.iterateEnum(UserBranch)
      },
      year     : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(UserYear)
      },
      college  : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      role     : {
        type     : Sequelize.STRING,
        allowNull: true,
        values   : Helpers.iterateEnum(UserRole)
      },
      leader_id: {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "users",
          key  : "id",
        },
        onDelete  : "cascade"
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("users"),
    ]);
  }
};
