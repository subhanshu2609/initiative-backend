import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up  : (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.addColumn("users", "team_id", {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "teams",
          key  : "id",
        },
        onDelete  : "cascade"
      })
    ]);
  },
  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.removeColumn("users", "team_id"),
    ]);
  }
};
