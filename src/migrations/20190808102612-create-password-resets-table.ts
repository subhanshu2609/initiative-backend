import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up  : (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("password_resets", {
      id       : {
        allowNull    : false,
        autoIncrement: true,
        primaryKey   : true,
        type         : Sequelize.BIGINT
      },
      email    : {
        allowNull: false,
        type     : Sequelize.STRING,
      },
      token    : {
        allowNull: false,
        type     : Sequelize.STRING,
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
    });
  },
  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("password_resets")
    ]);
  }
};
