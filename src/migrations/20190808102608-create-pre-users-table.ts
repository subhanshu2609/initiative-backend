import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { UserYear } from "../enums/user-year.enum";
import { UserBranch } from "../enums/user-branch.enum";
import { UserRole } from "../enums/user-role.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("pre-users", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      email    : {
        type     : Sequelize.STRING,
        unique   : true,
        allowNull: false
      },
      token    : {
        type     : Sequelize.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("pre-users"),
    ]);
  }
};
