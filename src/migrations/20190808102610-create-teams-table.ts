import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("teams", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      team_code  : {
        allowNull: true,
        type     : Sequelize.STRING
      },
      name     : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      topic    : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      marks    : {
        type     : Sequelize.INTEGER,
        allowNull: true
      },
      leader_id: {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "users",
          key  : "id",
        },
        onDelete  : "cascade"
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("teams"),
    ]);
  }
};
