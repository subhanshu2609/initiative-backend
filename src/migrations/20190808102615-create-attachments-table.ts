import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("attachments", {
      id            : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      type          : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      attachment_url: {
        type     : Sequelize.STRING,
        allowNull: false
      },
      team_id       : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "teams",
          key  : "id",
        },
        onDelete  : "cascade"
      },
      createdAt     : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt     : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt     : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("attachments"),
    ]);
  }
};
