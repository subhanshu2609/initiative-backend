import { UserBranch } from "../../enums/user-branch.enum";
import { UserYear } from "../../enums/user-year.enum";

export interface TeamCreateDto {
  name?: string;
  topic: string;
  leader_id?: number;
  team_code?: string;
}
