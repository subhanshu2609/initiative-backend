import { UserBranch } from "../../enums/user-branch.enum";
import { UserYear } from "../../enums/user-year.enum";

export interface TeamUpdateDto {
  name?: string;
  topic?: string;
}
