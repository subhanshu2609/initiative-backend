export interface AttachmentCreateDto {
  type: string;
  attachment_url: string;
  team_id?: number;
}
