import { UserBranch } from "../../enums/user-branch.enum";
import { UserYear } from "../../enums/user-year.enum";
import { UserRole } from "../../enums/user-role.enum";

export interface UserCreateDto {
  name: string;
  email: string;
  password?: string;
  mobile_no: string;
  roll_no: string;
  branch: UserBranch;
  year: UserYear;
  college: string;
  team_name?: string;
  role?: UserRole;
  leader_id?: number;
  topic_name?: string;
}
