import { UserBranch } from "../../enums/user-branch.enum";
import { UserYear } from "../../enums/user-year.enum";

export interface UserUpdateDto {
  name?: string;
  email?: string;
  password?: string;
  mobile_no?: string;
  roll_no?: string;
  branch?: UserBranch;
  year?: UserYear;
  college?: string;
  team_code?: string;
  team_id?: number;
}
